import { ProjectsService } from './../projects.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {

  constructor(private projectService:ProjectsService,
    private router:Router,private route:ActivatedRoute,
    public authService:AuthService) { }
    
  
    ProjectID:string;
    PorYYYYWW:string;
    Drop:string = "";
    DropType:string = "";
    kpi1:string;
    kpi2:string;
    kpi3:string;
    kpi4:string;
    kpi5:string;
    id:string;
    userID:string;
   
  ngOnInit() {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        console.log("The userID: ",this.userID)
      }
     )
  }

  onSubmit(){ 
    
    if(this.ProjectID ==null || this.PorYYYYWW ==null  || this.Drop ==""  || this.DropType==""  || this.kpi1==null  ||
      this.kpi2==null  || this.kpi3==null  || this.kpi4==null  || this.kpi5 ==null ){
        alert("All fields in this form are required")
      }
      else{
      this.projectService.addproject(this.ProjectID ,this.PorYYYYWW, this.Drop, this.DropType, this.kpi1,
        this.kpi2,this.kpi3,this.kpi4,this.kpi5,this.userID)
        this.router.navigate(['/projects']);}
    } 

}
