import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  constructor(private http:HttpClient, private db:AngularFirestore,public router:Router) { }
  private url = "https://97oli4muhj.execute-api.us-east-1.amazonaws.com/ALPHA";
  managersCollection: AngularFirestoreCollection = this.db.collection('managers');
  public categories:object = {0: 'IS LATE', 1: 'ON TIME'};
  public doc:string;


  classify():Observable<any>{
    let json = {
      "articles": [
        {"text": this.doc}
      ]
    }
    
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        return final;
      })
    )
  }

  // addClassification(userID:string, doc:string,category:string)
  // {
  //   const article={ userID:userID, doc:doc, category:category}

  //   this.managersCollection.doc(userID).collection('Articles').add(article);
  //   console.log("article: " ,article)
  //   this.router.navigate(['/articles'])    
  // }
  

}


