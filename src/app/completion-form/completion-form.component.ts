import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './../projects.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-completion-form',
  templateUrl: './completion-form.component.html',
  styleUrls: ['./completion-form.component.css']
})
export class CompletionFormComponent implements OnInit {

  constructor( private projectService:ProjectsService,
    public authService: AuthService, private router:Router, private route:ActivatedRoute, public classifyService:ClassifyService,) { }
    
    currentDate = new Date();
    userID;
    milestone$ : Observable<any>;
    milestoneID;
    Drop;
    DropType;
    PorYYYYWW;
    ProjectID;
    kpi1;
    kpi2;
    kpi3;
    kpi4;
    kpi5;
    Details$;
   
    
  ngOnInit() {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        this.Details$= this.projectService.getDetails(this.userID);
        this.milestoneID=this.route.snapshot.params.key;
      //  this.result = "IsLate" // wait for model
        console.log("The userID: ",this.userID)
        console.log("The milestoneID:" + this.milestoneID)

        this.projectService.getOnemilestone(this.userID,this.milestoneID).subscribe(
        milestone =>
        {
          console.log("this is the milestone.data Drop: ",milestone.data().Drop)
          console.log("this is the milestone.data DropType: ",milestone.data().DropType)
          console.log("this is the milestone.data PorYYYYWW: ",milestone.data().PorYYYYWW)
          console.log("this is the milestone.data ProjectID: ",milestone.data().ProjectID)
          console.log("this is the milestone.data kpi1: ",milestone.data().kpi1)
          console.log("this is the milestone.data kpi2: ",milestone.data().kpi2)
          console.log("this is the milestone.data kpi3: ",milestone.data().kpi3)
          console.log("this is the milestone.data kpi4: ",milestone.data().kpi4)
          console.log("this is the milestone.data kpi5: ",milestone.data().kpi5)

          this.Drop = milestone.data().Drop;
          this.DropType = milestone.data().DropType;
          this.PorYYYYWW = milestone.data().PorYYYYWW;
          this.ProjectID = milestone.data().ProjectID;
          this.kpi1 = milestone.data().kpi1;
          this.kpi2 = milestone.data().kpi2;
          this.kpi3 = milestone.data().kpi3;
          this.kpi4 = milestone.data().kpi4;
          this.kpi5 = milestone.data().kpi5;
         
        }
      );
      }
     )

  }

  private selectedLink: string="ON TIME";        
  

  setradio(e: string): void   
  {  
  
    this.selectedLink = e;  
          
  }  
  
    isSelected(name: string): boolean   
  {  
  
        if (!this.selectedLink) { // if no radio button is selected, always return false so every nothing is shown  
            return false;  
  }  
  
        return (this.selectedLink === name); // if current radio button is selected, return true, else return false  
    } 
  saveToHistory()
  {
    console.log(this.selectedLink)
    this.projectService.saveToHistory(this.userID, this.milestoneID, this.ProjectID, this.PorYYYYWW, this.Drop, this.DropType, this.selectedLink, this.currentDate, this.kpi1, this.kpi2, this.kpi3, this.kpi4, this.kpi5 )
    this.router.navigate(['/projects/']);
  }

  }


