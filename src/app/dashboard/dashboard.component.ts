import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ProjectsService } from './../projects.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { count, concatAll, shareReplay } from 'rxjs/operators';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 
  userID;
  LineChart = [];
  BarChart = [];
  PieChart = [];
  
  //History =  [{Status:"ON TIME"}, {Status:"ON TIME"}, {Status:"IS LATE"}, {Status:"IS LATE"}, {Status:"ON TIME"}];
    
    History$;
    saved$;
    Shered$;
    current$;
    ISLATE = 0;
    ONTIME = 0;
    savedlate = 0;
    savedtime = 0;
    Current = 0;
    History = 0;
    Saved = 0;
    Shered = 0;

  constructor(private projectService:ProjectsService,
    public authService: AuthService, private router:Router) { }
     
 

    pieChart(){
   // pie chart:
   this.PieChart = new Chart('pieChart', {
    type: 'pie',
data: {
labels: ["IS LATE", "ON TIME"],
datasets: [{
    data: [this.ISLATE, this.ONTIME],
    backgroundColor: [
        '	rgb(255,0,0, 0.75)', //optional ,0.1-1 שקיפות
        'rgb(34,139,34, 0.75)'
    ],
    borderColor: [
        'rgba(0,0,0)',
        'rgba(0,0,0)',

    ],
    borderWidth: 1
}]
}, 
options: {
title:{
    text: "Distribution of my history project",
    display:true
},
scales: {
    yAxes: [{
        ticks: {
            beginAtZero: true,
            display:false,
            
        }
    }]
}
}
});
    }

    barChart(){
        this.BarChart = new Chart('barChart', {
            type: 'bar',
          data: {
           datasets: [
               {
               label: "IS LATE",
               data: [this.savedlate],
               backgroundColor: [
                'rgb(255,0,0, 0.75)', //optional ,0.1-1 שקיפות
               ],
               borderColor: [
                'rgba(0,0,0)',
               ],
               borderWidth: 1
           },
           {
            label: "ON TIME",
            data: [this.savedtime],
            backgroundColor: [
             'rgb(34,139,34, 0.75)'
            ],
            borderColor: [
             'rgba(0,0,0)',
            ],
            borderWidth: 1
        }
        ]
          }, 
          options: {
           title:{
               text:"Distribution of my saved forcast",
               display:true
           },
           scales: {
               yAxes: [{
                   ticks: {
                       beginAtZero:true
                   } 
               }] 
           }
          }
          });
         }
    

  ngOnInit() {
    this.authService.getUser().subscribe(
         (user) =>
        (
          this.userID=user.uid,
         //this.History$ = this.projectService.getHistory(this.userID)
         this.History$ = this.projectService.getHistory(this.userID).subscribe(
             (res) => (
                 res.forEach(element => {
                     this.History = this.History + 1;
                     if(element.Status == "ON TIME"){
                        this.ONTIME = this.ONTIME + 1
                        console.log("this is ONTIME", this.ONTIME)
                     } 
                     else{
                         this.ISLATE = this.ISLATE + 1
                         console.log("this ISLATE", this.ISLATE)
                     }
                 }
                    ),
                    this.pieChart()
             )
         ),

                  this.saved$ = this.projectService.getsaved(this.userID).subscribe(
             (res) => (
                 res.forEach(element => {
                    this.Saved = this.Saved + 1;
                     console.log("element", element)
                     if(element.result == "ON TIME"){
                        this.savedtime = this.savedtime + 1
                        console.log("this is savedtime", this.savedtime)
                     } 
                     else{
                         this.savedlate = this.savedlate + 1
                         console.log("this savedlate", this.savedlate)
                     }
                 }
                    ),
                    this.barChart()
             )
         ),

         this.current$ = this.projectService.getMilestones(this.userID).subscribe(
            (res) => (
                res.forEach(element => {
                    console.log("element", element)
                    this.Current = this.Current + 1;
                }
                   )
            )
        ),

        this.Shered$ = this.projectService.getShered(this.userID).subscribe(
            (res) => (
                res.forEach(element => {
                    console.log("element", element)
                    this.Shered = this.Shered + 1;
                }
                   )
            )
        )
         
  )
  )   
   }
  }
