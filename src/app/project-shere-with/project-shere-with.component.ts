import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../projects.service';
import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-shere-with',
  templateUrl: './project-shere-with.component.html',
  styleUrls: ['./project-shere-with.component.css']
})
export class ProjectShereWithComponent implements OnInit {

  constructor(private projectService:ProjectsService,
    public authService: AuthService, private router:Router, private route:ActivatedRoute) { }


    users$ : Observable<any>; 
    userID;
    from;
    category;
    result;
    milestone$ : Observable<any>;
    milestoneID;
    Drop;
    DropType;
    PorYYYYWW;
    ProjectID;
    currentDate = new Date();
    kpi1;
    kpi2;
    kpi3;
    kpi4;
    kpi5;

  ngOnInit() {
    this.from = this.route.snapshot.params.from;
    console.log("i know the name", this.from)
     this.users$ = this.projectService.getAllUsers();
      }
   

  onSubmit(){ 
    console.log("in onSubmit Shere")
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        this.milestoneID=this.route.snapshot.params.milestone_id;
        this.result = this.route.snapshot.params.result;
        this.from = this.route.snapshot.params.from;
        console.log("The userID: ",this.userID)
        console.log("The milestoneID:" + this.milestoneID)
        console.log("The result:" + this.result)


        this.projectService.getOnemilestone(this.userID,this.milestoneID).subscribe(
        milestone =>
        {
          console.log("this is the milestone.data Drop: ",milestone.data().Drop)
          console.log("this is the milestone.data DropType: ",milestone.data().DropType)
          console.log("this is the milestone.data PorYYYYWW: ",milestone.data().PorYYYYWW)
          console.log("this is the milestone.data ProjectID: ",milestone.data().ProjectID)
          console.log("this is the milestone.data kpi1: ",milestone.data().kpi1)
          console.log("this is the milestone.data kpi2: ",milestone.data().kpi2)
          console.log("this is the milestone.data kpi3: ",milestone.data().kpi3)
          console.log("this is the milestone.data kpi4: ",milestone.data().kpi4)
          console.log("this is the milestone.data kpi5: ",milestone.data().kpi5)

          this.Drop = milestone.data().Drop;
          this.DropType = milestone.data().DropType;
          this.PorYYYYWW = milestone.data().PorYYYYWW;
          this.ProjectID = milestone.data().ProjectID;
          this.kpi1 = milestone.data().kpi1;
          this.kpi2 = milestone.data().kpi2;
          this.kpi3 = milestone.data().kpi3;
          this.kpi4 = milestone.data().kpi4;
          this.kpi5 = milestone.data().kpi5;
          this.projectService.addShere(this.milestoneID, this.ProjectID, this.PorYYYYWW, this.Drop, this.DropType, this.currentDate,this.kpi1, this.kpi2, this.kpi3, this.kpi4, this.kpi5, this.from, this.category, this.result)
          this.router.navigate(['/predict']);
        }
      );

      }
     )

   
  
  }


}
