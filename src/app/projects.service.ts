import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  // userCollection: AngularFirestoreCollection= this.db.collection('managers');
  MilestonesCollection: AngularFirestoreCollection;
  DetailsCollection: AngularFirestoreCollection;
  UsersCollection: AngularFirestoreCollection;
  ShereCollection: AngularFirestoreCollection;
  HistoryCollection: AngularFirestoreCollection;



  constructor(public db:AngularFirestore,
              public authService:AuthService,) { }

              getMilestones(userID): Observable<any[]> 
    {
      this.MilestonesCollection = this.db.collection(`managers/${userID}/milestones`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.MilestonesCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

    addproject(ProjectID:string,
      PorYYYYWW:string,
      Drop:string,
      DropType:string,
      kpi1:string,
      kpi2:string,
      kpi3:string,
      kpi4:string,
      kpi5:string,
      userID:string
      ){
      const project = {ProjectID:ProjectID, PorYYYYWW:PorYYYYWW, Drop:Drop, DropType:DropType, kpi1:kpi1,kpi2:kpi2,
        kpi3:kpi3,kpi4:kpi4,kpi5:kpi5, userID:userID}
      this.db.collection(`managers/${userID}/milestones`).add(project);
    }

    getOnemilestone(userID:string, milestoneID:string):Observable<any>
    {
      console.log("in getOnemilestone milestoneID: ",milestoneID)
      console.log("in getOnemilestone userID: ",userID)
      console.log("search milestone: ",(this.db.doc(`managers/${userID}/milestones/${milestoneID}`).get()));
      return this.db.doc(`managers/${userID}/milestones/${milestoneID}`).get();
      
    }

    

    getDetails(userID): Observable<any[]> 
    {
      this.DetailsCollection = this.db.collection(`managers/${userID}/Details`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.DetailsCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

  
    getAllUsers(): Observable<any[]> 
    {
      this.UsersCollection = this.db.collection(`users`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.UsersCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

    addShere(
      milestoneID:string,
      ProjectID:string,
      PorYYYYWW:string,
      Drop:string,
      DropType:string,
      currentDate:Date,
      kpi1:string,
      kpi2:string,
      kpi3:string,
      kpi4:string,
      kpi5:string,
      from:string,
      to:string,
      result:string,
    
      ){
        console.log("in addShere ")
      const project = { from:from, milestoneID:milestoneID, ProjectID:ProjectID,PorYYYYWW:PorYYYYWW, Drop:Drop, DropType:DropType,kpi1:kpi1,kpi2:kpi2,
        kpi3:kpi3,kpi4:kpi4,kpi5:kpi5,result:result, Date_Time:currentDate }
      //this.db.collection('books').add(book)
      this.db.collection(`shered/${to}/milestones`).add(project);
    }


    getShered(userID): Observable<any[]> 
    {
      this.ShereCollection = this.db.collection(`shered/${userID}/milestones`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.ShereCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

    addClassification(userID:string, milestoneID:string, ProjectID:string, PorYYYYWW:string, Drop:string, DropType:string, result:string, currentDate:Date, kpi1:string, kpi2:string, kpi3:string, kpi4:string, kpi5:string){
      console.log("in addClassification")
      const project = {Date_Time:currentDate, milestoneID:milestoneID, ProjectID:ProjectID,PorYYYYWW:PorYYYYWW, Drop:Drop, DropType:DropType, result:result, kpi1:kpi1,kpi2:kpi2,
        kpi3:kpi3,kpi4:kpi4,kpi5:kpi5, userID:userID}
      //this.db.collection('books').add(book)
      this.db.collection(`managers/${userID}/saved_project`).add(project);
    
     }



    getSeved(userID): Observable<any[]> 
    {
      this.ShereCollection = this.db.collection(`managers/${userID}/saved_project`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.ShereCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 
    
    deleteSaved(userID, key_id)
    {
      this.db.doc(`managers/${userID}/saved_project/${key_id}`).delete();
      console.log(key_id+" have deleted")
    }

    
    deleteShered(userID, key_id)
    {
      this.db.doc(`shered/${userID}/milestones/${key_id}`).delete();
      console.log(key_id+" have deleted")
    }

    

    saveToHistory(userID:string, milestoneID:string, ProjectID:string, PorYYYYWW:string, Drop:string, DropType:string, Status:string, currentDate:Date, kpi1:string, kpi2:string, kpi3:string, kpi4:string, kpi5:string){
      console.log("in saveToHistory")
      const project = {Date_Time:currentDate, milestoneID:milestoneID, ProjectID:ProjectID,PorYYYYWW:PorYYYYWW, Drop:Drop, DropType:DropType, Status:Status, kpi1:kpi1, kpi2:kpi2, kpi3:kpi3, kpi4:kpi4, kpi5:kpi5, userID:userID}
      this.db.collection(`managers/${userID}/History_projects`).add(project).then(
        (res) => 
        {
        this.db.doc(`managers/${userID}/milestones/${milestoneID}`).delete()
        }
      )
    }

    deleteHistory(userID, key_id)
    {
      this.db.doc(`managers/${userID}/History_projects/${key_id}`).delete();
      console.log(key_id+" have deleted")
    }

    getHistory(userID): Observable<any[]> 
    {
      this.HistoryCollection = this.db.collection(`managers/${userID}/History_projects`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.HistoryCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

    getsaved(userID): Observable<any[]>{
      this.HistoryCollection = this.db.collection(`managers/${userID}/saved_project`);
      //console.log("this.articleCollection: ",this.articleCollection)
      return this.HistoryCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 


}
