// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDWNAtxBww56gKE5YRBSeDJUMqwwq71pgo",
    authDomain: "systemdemo-c2e07.firebaseapp.com",
    databaseURL: "https://systemdemo-c2e07.firebaseio.com",
    projectId: "systemdemo-c2e07",
    storageBucket: "systemdemo-c2e07.appspot.com",
    messagingSenderId: "800053715031",
    appId: "1:800053715031:web:e4f95b0ffba533e285c312"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
